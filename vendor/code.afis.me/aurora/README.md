Aurora
======

#### Ultimate ANSI colors for Golang. The package supports Printf/Sprintf etc.

### Installation

```sh
$ go get -u code.afis.me/aurora
```
### Usage

```go
package main

import (
	"fmt"

	. "code.afis.me/aurora"
)

func main() {
	fmt.Println("Hello,", Magenta("Aurora"))
	fmt.Println(Bold(Cyan("Cya!")))
}

```

```sh
https://github.com/logrusorgru/aurora
```
