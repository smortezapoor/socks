package logging

import (
	"crypto/md5"
	cryptorand "crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"time"

	"code.afis.me/aurora"
)

//Logger -
type Logger struct {
	Name          string
	Splitter      string
	LogLevel      int
	OutputHandler func(*PushLogs)
}

const (
	// LOGDEBUG -
	LOGDEBUG = iota
	// LOGWARNING -
	LOGWARNING
	// LOGERROR -
	LOGERROR
	// LOGSUCCESS -
	LOGSUCCESS
)

const (
	// LEVELDEBUG -
	LEVELDEBUG = iota
	// LEVELWARNING -
	LEVELWARNING
	// LEVELERROR -
	LEVELERROR
)

func init() {
	log.SetFlags(log.Lshortfile)
}

//PushLogs -
type PushLogs struct {
	ID         string    `json:"id"`
	Origin     string    `json:"origin"`
	Name       string    `json:"name"`
	Time       time.Time `json:"time"`
	Level      int       `json:"level"`
	LongFile   string    `json:"longFile"`
	ShortFile  string    `json:"shortFile"`
	LineNumber int       `json:"lineNumber"`
	Message    string    `json:"message"`
}

//NewLogger -
func NewLogger(name, spli string) *Logger {
	return &Logger{Name: name, Splitter: spli}
}

//GetLevelLogName -
func GetLevelLogName(level int) (lv string) {
	switch level {
	case 0:
		lv = "DEBUG"
		break
	case 1:
		lv = "WARNING"
		break
	case 2:
		lv = "ERROR"
		break
	case 3:
		lv = "SUCCESS"
		break
	}
	return lv
}

//OutputHandlerConsole -
func OutputHandlerConsole(ps *PushLogs) {
	var ems aurora.Value
	var vms aurora.Value

	switch ps.Level {
	case 0:
		ems = aurora.Bold(GetLevelLogName(ps.Level)).Blue()
		vms = aurora.Bold(ps.Message).White()
		break
	case 1:
		ems = aurora.Bold(GetLevelLogName(ps.Level)).Yellow()
		vms = aurora.Bold(ps.Message).Yellow()
		break
	case 2:
		ems = aurora.Bold(GetLevelLogName(ps.Level)).Red()
		vms = aurora.Bold(ps.Message).Red()
		break
	case 3:
		ems = aurora.Bold(GetLevelLogName(ps.Level)).Green()
		vms = aurora.Bold(ps.Message).Green()
		break

	}

	fmt.Println(fmt.Sprintf("[%s][%s][%s][%d][%s] %s", aurora.Cyan(ps.Name),
		ems, aurora.Magenta(ps.Time.Format("2 Jan 2006 15:04:05")),
		aurora.BrightCyan(ps.LineNumber), aurora.BrightCyan(ps.ShortFile), vms))
}

//SetLevel -
func (cc *Logger) SetLevel(ps int) *Logger {
	cc.LogLevel = ps
	return cc
}

//Output -
func (cc *Logger) Output(ps *PushLogs) *Logger {

	if cc.OutputHandler == nil {
		OutputHandlerConsole(ps)
		return cc
	}

	cc.OutputHandler(ps)
	return cc
}

//Quit -
func (cc *Logger) Quit(err error) error {
	if err == nil {
		return nil
	}

	os.Exit(2)

	return err
}

//AlwaysShow -
func (cc *Logger) AlwaysShow(level int, msg string) *Logger {

	pc, _, line, _ := runtime.Caller(1)
	file, _ := runtime.FuncForPC(pc).FileLine(pc)
	sp2 := strings.Split(file, cc.Splitter)
	shortFile := strings.Join(sp2, "")

	if len(sp2) < 2 {
		sp2 = strings.Split(file, "/go/src/")
	}

	if len(sp2) > 1 {
		shortFile = sp2[1]
	}

	cc.Output(&PushLogs{Name: cc.Name, ID: cc.MakeID(), Time: time.Now(), Level: level, LongFile: file, ShortFile: shortFile, LineNumber: line, Message: msg})
	return cc
}

//Debug -
func (cc *Logger) Debug(msg string) *Logger {
	if cc.LogLevel == LEVELWARNING || cc.LogLevel == LEVELERROR {
		return cc
	}

	pc, _, line, _ := runtime.Caller(1)
	file, _ := runtime.FuncForPC(pc).FileLine(pc)
	sp2 := strings.Split(file, cc.Splitter)
	shortFile := strings.Join(sp2, "")

	if len(sp2) < 2 {
		sp2 = strings.Split(file, "/go/src/")
	}

	if len(sp2) > 1 {
		shortFile = sp2[1]
	}

	cc.Output(&PushLogs{Name: cc.Name, ID: cc.MakeID(), Time: time.Now(), Level: LOGDEBUG, LongFile: file, ShortFile: shortFile, LineNumber: line, Message: msg})
	return cc
}

//Warning -
func (cc *Logger) Warning(msg string) *Logger {
	if cc.LogLevel == LEVELERROR {
		return cc
	}

	pc, _, line, _ := runtime.Caller(1)
	file, _ := runtime.FuncForPC(pc).FileLine(pc)
	sp2 := strings.Split(file, cc.Splitter)
	shortFile := strings.Join(sp2, "")
	if len(sp2) < 2 {
		sp2 = strings.Split(file, "/go/src/")
	}
	if len(sp2) > 1 {
		shortFile = sp2[1]
	}

	cc.Output(&PushLogs{Name: cc.Name, ID: cc.MakeID(), Time: time.Now(), Level: LOGWARNING, LongFile: file, ShortFile: shortFile, LineNumber: line, Message: msg})
	return cc
}

//Success -
func (cc *Logger) Success(msg string) *Logger {
	if cc.LogLevel == LEVELWARNING || cc.LogLevel == LEVELERROR {
		return cc
	}

	pc, _, line, _ := runtime.Caller(1)
	file, _ := runtime.FuncForPC(pc).FileLine(pc)
	sp2 := strings.Split(file, cc.Splitter)
	shortFile := strings.Join(sp2, "")
	if len(sp2) < 2 {
		sp2 = strings.Split(file, "/go/src/")
	}
	if len(sp2) > 1 {
		shortFile = sp2[1]
	}

	cc.Output(&PushLogs{Name: cc.Name, ID: cc.MakeID(), Time: time.Now(), Level: LOGSUCCESS, LongFile: file, ShortFile: shortFile, LineNumber: line, Message: msg})
	return cc
}

func (cc *Logger) Error(err error) *Logger {

	if err == nil {
		return cc
	}

	pc, _, line, _ := runtime.Caller(1)
	file, _ := runtime.FuncForPC(pc).FileLine(pc)
	sp2 := strings.Split(file, cc.Splitter)

	shortFile := strings.Join(sp2, "")
	if len(sp2) < 2 {
		sp2 = strings.Split(file, "/go/src/")
	}
	if len(sp2) > 1 {
		shortFile = sp2[1]
	}

	cc.Output(&PushLogs{Name: cc.Name, ID: cc.MakeID(), Time: time.Now(), Level: LOGERROR, LongFile: file, ShortFile: shortFile, LineNumber: line, Message: fmt.Sprintf("%s", err)})

	return cc
}

//MakeID -
func (cc *Logger) MakeID() string {
	return cc.CreateHash(cc.GenerateUUID())
}

//CreateHash -
func (cc *Logger) CreateHash(key string) string {

	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()

	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

//GenerateUUID -
func (cc *Logger) GenerateUUID() string {

	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()

	uuid := make([]byte, 16)
	n, err := io.ReadFull(cryptorand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return ""
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}
