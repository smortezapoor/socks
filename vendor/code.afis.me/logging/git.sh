#!/bin/bash
NOW=$(date +"%A, %d %B %Y %T")
init=false
commit=false
message="Auto commit on $NOW"

for arg in "$@"
do
    case $arg in
        -init)
            init=true
        shift
        ;;
        -commit)
            commit=true
        shift
        ;;
        -message)
            message="$2"
        shift
        shift
        ;;
    esac
done


if [[ $init == true ]]; then
    echo "Removing old local git" && rm -rf .git
    echo "Init git"
    git init
    git fetch --all
    git checkout -B master
    git remote add origin git@gitlab.com:afis/logging.git
    if [[ "$(git add -A && git commit -a -m 'first commit')" != *"nothing to commit"* ]]; then
        git push -f origin master
    fi
fi


if [[ $commit == true ]]; then
    if [[ "$(git add -A && git commit -a -m "$message")" != *"nothing to commit"* ]]; then
       git push -u origin master
    fi
fi