##  Small Golang package to output alternative logs



### Installation ###

```sh
$ go get code.afis.me/logging
```

### Example ###

```go
package main

import (
	"code.afis.me/logging"
)

var logger *logging.Logger

func init() {
	logger = logging.NewLogger("trading.desktop", "")
}


func main() {
		
	err := fmt.Errorf("Hello")
	
	// just display error output
	logger.Error(err)
	
	// if error not nil, then run os.Exit(2)
	logger.Error(err).Quit(err)
	
	logger.Debug("Hello from golang")
	logger.Warning("Hello from golang")
	logger.Success("Hello from golang")
	
}
```
