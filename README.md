# Socks Proxy Server

A light-weight socks proxy server. Only support SOCKS5 Protocol

## Compile and building

```sh
go get code.afis.me/socks && make
# binary available under bin directory
```

### Install using snap
```sh
# https://snapcraft.io/socks-proxy
sudo snap install socks-proxy
# database location base on home directory, if you run in daemon, run with sudo
# list users
sudo socks-proxy --listuser
# change user password
sudo socks-proxy --updateuser
# add user
sudo socks-proxy --adduser
# delete user
sudo socks-proxy --deleteuser
```

### Complete command
```sh
sudo socks-proxy -h
```