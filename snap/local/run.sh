#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

dev=false

for arg in "$@"
do
    case $arg in
        -dev)
            dev=true
        shift
        ;;
    esac
done


export GODEBUG="madvdontneed=1"
export REALHOME=`getent passwd $UID | cut -d ':' -f 6`


if [[ $dev == false ]]; then
    exec "$SNAP/bin/socks-proxy" "$@"
fi

if [[ $dev == true ]]; then
    exec "/home/afis/Project/go/src/code.afis.me/socks/bin/socks-proxy" "$@"
fi

