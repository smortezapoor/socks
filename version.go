package main

import (
	"fmt"
	"html/template"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"code.afis.me/aurora"
)

const verboseVersionBanner string = `%s
├── %s%v: {{ .AppVersion }}
├── %s%v: {{ .GitCommit }}
├── %s%v: {{ .GoVersion }}
├── %s%v: {{ .Compiler }}
└── %s%v: {{ .BuildTime }}
`

func showVersion() {
	if len(BuildTime) == 0 {
		BuildTime = "0"
	}

	if bt, err := strconv.ParseInt(BuildTime, 10, 64); err == nil {

		bannervar := map[string]aurora.Value{
			"AppVersion": aurora.Bold(AppVersion).Cyan(),
			"GitCommit":  aurora.Bold(GitCommit).Cyan(),
			"GoVersion":  aurora.Bold(fmt.Sprintf("%s %s/%s", strings.Replace(runtime.Version(), "go", "", -1), runtime.GOOS, runtime.GOARCH)).Cyan(),
			"Compiler":   aurora.Bold(fmt.Sprintf("%s (%s)", OSBuildVersion, runtime.Compiler)).Cyan(),
			"BuildTime":  aurora.Bold(time.Unix(bt, 0).Format("Mon Jan 02 15:04:05 MST 2006")).Cyan(),
		}

		t := template.Must(template.New("banner").
			Parse(fmt.Sprintf(verboseVersionBanner,
				aurora.Bold("A light-weight socks proxy server").Magenta(),
				aurora.Bold("Version").Blue(), "\t",
				aurora.Bold("GitCommit").Blue(), "\t",
				aurora.Bold("GoVersion").Blue(), "\t",
				aurora.Bold("Compiler").Blue(), "\t",
				aurora.Bold("BuildTime").Blue(), "\t",
			)))
		t.Execute(os.Stdout, bannervar)
	}

}
