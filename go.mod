module code.afis.me/socks

go 1.14

require (
	code.afis.me/aurora v0.0.0-20200818164603-cab410b5c657
	code.afis.me/logging v0.0.0-20200820154106-a0af418f03f5
	code.afis.me/websocket v0.0.0-20200820160855-d7f499bc3b90
	github.com/inhies/go-bytesize v0.0.0-20200716184324-4fe85e9b81b2
	github.com/karrick/golf v1.4.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/xujiajun/nutsdb v0.5.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
