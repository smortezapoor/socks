#!/bin/bash -e

dir="$(dirname $(realpath ${BASH_SOURCE[0]}))" && cd $dir
version="1.0.0"
commit=$(git rev-list -1 HEAD)    
buildtime="$(date +%s)"
os="$(lsb_release -si)"-$(lsb_release -sr)
cd $dir
finaltags='-linkmode external -extldflags "-lm -lstdc++ -static"'


rm -rf $dir/bin/
go build -tags "netgo $finaltags" -ldflags '-s -w -X main.OSBuildVersion='"$os"' -X main.AppVersion='"$version"' -X  main.BuildTime='"$buildtime"' -X main.GitCommit='"$commit" -o $dir/bin/socks-proxy