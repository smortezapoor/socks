package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"path"
	"runtime/debug"
	"strconv"
	"syscall"
	"time"

	"code.afis.me/logging"
	"code.afis.me/socks/lib"
	"github.com/karrick/golf"
	"github.com/mitchellh/go-homedir"
)

var logger *logging.Logger
var port, loglevel int
var address string
var allowAnonymous, version, help, listuser, adduser, updateuser, deleteuser bool
var pid int
var process *os.Process
var home string
var appID = "me.afis.app.socks.proxy"
var validprocess bool
var messages = make(chan string)
var db *lib.Database

// GitCommit every build
var GitCommit string

// AppVersion every build
var AppVersion string

// OSBuildVersion -
var OSBuildVersion string

// BuildTime -
var BuildTime string

func init() {
	logger = &logging.Logger{Name: "socks", Splitter: "/code.afis.me/socks/"}
	home, _ = homedir.Dir()
	home = path.Join(home, ".config", appID)

	if len(os.Getenv("FLATPAK_ID")) != 0 {
		appID = os.Getenv("FLATPAK_ID")
		home = os.Getenv("XDG_CONFIG_HOME")
	}

	// if run on snap runtime
	if len(os.Getenv("SNAP")) != 0 {
		appID = "me.afis.app.socks.proxy.snap"
		if len(os.Getenv("REALHOME")) == 0 {
			log.Panic("REALHOME is empty")
		}
		home = path.Join(os.Getenv("REALHOME"), "AppsData", "config", appID)
	}

	handleMaintSignals()
	memoryUsage()

}

var errs error

func main() {

	golf.BoolVarP(&help, 'h', "help", false, "Display command line help")
	golf.BoolVarP(&version, 'v', "version", false, "Show application version")
	golf.IntVarP(&loglevel, 'l', "loglevel", logging.LEVELERROR, "LogLevel, 0 for debug, 1 for warning, 2 for error")
	golf.IntVarP(&port, 'p', "port", 3131, "Sock5 server listening port")
	golf.StringVarP(&address, 'a', "address", "0.0.0.0", "Sock5 server listening address")
	golf.BoolVarP(&allowAnonymous, 'n', "anonymous", false, "Allow Anonymous")
	golf.BoolVar(&adduser, "adduser", false, "Add User to Database")
	golf.BoolVar(&updateuser, "updateuser", false, "Update User to Database")
	golf.BoolVar(&deleteuser, "deleteuser", false, "Delete User in Database")
	golf.BoolVar(&listuser, "listuser", false, "List User in Database")
	golf.Parse()

	if help {
		golf.Usage()
		return
	}

	logger.SetLevel(loglevel)

	showVersion()

	if version {
		return
	}

	db = lib.NewDatabase(home, logger)
	db.Init()

	errs = fmt.Errorf("No current process found")

	if listuser {
		logger.SetLevel(logging.LEVELDEBUG)
		clistuser()
		return
	}

	if adduser || updateuser || deleteuser {

		logger.SetLevel(logging.LEVELDEBUG)

		if val, err := db.Get("config", "pid", true); err == nil {
			if len(val) == 0 {
				logger.Error(errs).Quit(errs)
			}
			if pid, err = strconv.Atoi(string(val)); err != nil {
				logger.Error(errs).Quit(errs)
			}

			if process, err = os.FindProcess(pid); err != nil {
				logger.Error(errs).Quit(errs)
			}

		} else {
			logger.Error(err).Quit(err)
		}

		if process == nil {
			logger.Error(errs).Quit(errs)
		}

	}

	if adduser {
		cuaddeuser()
		return
	}

	if updateuser {
		cupdateuser()
		return
	}

	if deleteuser {
		cdeleteuser()
		return
	}

	srv := lib.New(logger, false)
	srv.AuthNoAuthenticationRequiredCallback = func(c *lib.Conn) error {
		if allowAnonymous {
			return nil
		}

		return lib.ErrAuthenticationFailed
	}
	srv.AuthUsernamePasswordCallback = func(c *lib.Conn, username, password []byte) error {
		user := string(username)
		passwd := string(password)

		if val, ok := db.Stores.Users.Load(user); ok {
			if val.Password != db.CreateHash(passwd) {
				return lib.ErrAuthenticationFailed
			}
		} else {
			return lib.ErrAuthenticationFailed
		}

		logger.Success(fmt.Sprintf("[%s][%s] Connected", c.RemoteAddr(), user))

		c.Data = user

		return nil
	}
	srv.HandleConnectFunc(func(c *lib.Conn, host string) (newHost string, err error) {
		if user, ok := c.Data.(string); ok {
			logger.Debug(fmt.Sprintf("[%s][%s] Connecting to %s", c.RemoteAddr(), user, host))
		} else {
			logger.Debug(fmt.Sprintf("[%s] Connecting to %s", c.RemoteAddr(), host))
		}

		return host, nil
	})
	srv.HandleCloseFunc(func(c *lib.Conn) {
		if user, ok := c.Data.(string); ok {
			logger.Warning(fmt.Sprintf("[%s][%s] Disconnected", c.RemoteAddr(), user))
		} else {
			logger.Warning(fmt.Sprintf("[%s] Disconnected", c.RemoteAddr()))
		}
	})

	go func() {
		time.Sleep(1 * time.Second)
		db.Set("config", "pid", []byte(fmt.Sprintf("%d", os.Getpid())), true)
	}()

	if err := srv.ListenAndServe(fmt.Sprintf("%s:%d", address, port)); err != nil {
		logger.Error(err).Quit(err)
	}
}

func handleMaintSignals() {
	ch := make(chan os.Signal, 1)
	go func() {
		for sig := range ch {
			switch sig {
			case syscall.SIGUSR1:
				db.Reload()
				if val, err := db.Get("config", "ppid", true); err == nil {
					if pid, err := strconv.Atoi(string(val)); err == nil {
						if process, err = os.FindProcess(pid); err == nil {
							process.Signal(syscall.SIGUSR2)
						}
					}
				}

			case syscall.SIGUSR2:
				messages <- ""
			}
		}
	}()
	signal.Notify(ch, syscall.SIGUSR1, syscall.SIGUSR2)
}

func memoryUsage() {
	debug.SetGCPercent(10)
	_ = os.Setenv("GODEBUG", "madvdontneed=1")
	go func() {
		ticker := time.NewTicker(30 * time.Second)
		for {
			select {
			case t := <-ticker.C:
				t.Second()
				debug.FreeOSMemory()
			}
		}
	}()
}
