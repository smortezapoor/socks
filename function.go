package main

import (
	"bufio"
	"fmt"
	"os"
	"syscall"
	"time"

	"code.afis.me/socks/lib"
	"golang.org/x/crypto/ssh/terminal"
)

func cupdateuser() {
	db.Set("config", "ppid", []byte(fmt.Sprintf("%d", os.Getpid())), true)
	fmt.Print("Enter username: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	username := scanner.Text()
	if len(username) == 0 {
		ers := fmt.Errorf("Username is required")
		logger.Error(ers).Quit(ers)
	}

	if val, ok := db.Stores.Users.Load(username); ok {
		fmt.Print("Enter new Password: ")
		newpassword, _ := terminal.ReadPassword(0)
		if len(newpassword) == 0 {
			fmt.Print("\n")
			ers := fmt.Errorf("Password is required")
			logger.Error(ers).Quit(ers)
		}

		fmt.Print("\nRepeat new Password: ")
		repeatnewpassword, _ := terminal.ReadPassword(0)

		if string(newpassword) != string(repeatnewpassword) {
			fmt.Print("\n")
			ers := fmt.Errorf("Password not match")
			logger.Error(ers).Quit(ers)
		}

		fmt.Print("\n")

		val.Password = db.CreateHash(string(newpassword))
		db.ChangeUsers(val)

	} else {
		ers := fmt.Errorf("Username not found")
		logger.Error(ers).Quit(ers)
	}

	process.Signal(syscall.SIGUSR1)

	go func() {
		timeout := time.After(5 * time.Second)
		for {
			select {
			case <-timeout:
				logger.Error(errs).Quit(errs)
			default:

			}
		}
	}()

	<-messages
	logger.Success(fmt.Sprintf("User '%s' has been updated", username))
	return
}

func cuaddeuser() {
	db.Set("config", "ppid", []byte(fmt.Sprintf("%d", os.Getpid())), true)
	fmt.Print("Enter username: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	username := scanner.Text()
	if len(username) == 0 {
		ers := fmt.Errorf("Username is required")
		logger.Error(ers).Quit(ers)
	}

	if val, ok := db.Stores.Users.Load(username); ok {
		ers := fmt.Errorf("Username %s already in database", val.UserName)
		logger.Error(ers).Quit(ers)
	}

	fmt.Print("Enter new Password: ")
	newpassword, _ := terminal.ReadPassword(0)
	if len(newpassword) == 0 {
		fmt.Print("\n")
		ers := fmt.Errorf("Password is required")
		logger.Error(ers).Quit(ers)
	}

	fmt.Print("\nRepeat new Password: ")
	repeatnewpassword, _ := terminal.ReadPassword(0)

	if string(newpassword) != string(repeatnewpassword) {
		fmt.Print("\n")
		ers := fmt.Errorf("Password not match")
		logger.Error(ers).Quit(ers)
	}

	fmt.Print("\n")

	db.ChangeUsers(lib.Users{
		UserName: username,
		Password: db.CreateHash(string(newpassword)),
	})

	process.Signal(syscall.SIGUSR1)

	go func() {
		timeout := time.After(5 * time.Second)
		for {
			select {
			case <-timeout:
				logger.Error(errs).Quit(errs)
			default:

			}
		}
	}()

	<-messages
	logger.Success(fmt.Sprintf("User '%s' has been added", username))
	return
}

func clistuser() {
	for _, s := range db.Stores.Users.LoadAll() {
		fmt.Println(s.UserName)
	}
	return
}

func cdeleteuser() {

	db.Set("config", "ppid", []byte(fmt.Sprintf("%d", os.Getpid())), true)

	if db.Stores.Users.Count() <= 1 {
		ers := fmt.Errorf("Min users is 1")
		logger.Error(ers).Quit(ers)
	}

	fmt.Print("Enter username: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	username := scanner.Text()
	if len(username) == 0 {
		ers := fmt.Errorf("Username is required")
		logger.Error(ers).Quit(ers)
	}

	if _, ok := db.Stores.Users.Load(username); !ok {
		ers := fmt.Errorf("Username '%s' not found in database", username)
		logger.Error(ers).Quit(ers)
	}

	db.DeleteUsers(username)

	process.Signal(syscall.SIGUSR1)

	go func() {
		timeout := time.After(5 * time.Second)
		for {
			select {
			case <-timeout:
				logger.Error(errs).Quit(errs)
			default:

			}
		}
	}()

	<-messages
	logger.Success(fmt.Sprintf("User '%s' has been deleted", username))
	return
}
