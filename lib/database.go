package lib

import (
	"bytes"
	"crypto/md5"
	"encoding/gob"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"sync"

	"code.afis.me/logging"
	"github.com/xujiajun/nutsdb"
)

//Database -
type Database struct {
	Location string
	Logger   *logging.Logger
	Stores   Stores
	DB       *nutsdb.DB
}

//Stores -
type Stores struct {
	Users *UsersMap
}

//Users -
type Users struct {
	UserName string
	Password string
}

//NewDatabase -
func NewDatabase(loc string, lo *logging.Logger) *Database {
	lo.Debug(fmt.Sprintf("Using database location %s", loc))
	db := &Database{Location: loc, Logger: lo}
	db.Stores.Users = NewUsersMap()
	return db
}

//Init -
func (cc *Database) Init() (err error) {

	if err := os.MkdirAll(path.Join(cc.Location, "socks"), 0755); cc.Logger.Error(err).Quit(err) != nil {

	}

	opt := nutsdb.DefaultOptions
	opt.Dir = path.Join(cc.Location, "socks")
	if cc.DB, err = nutsdb.Open(opt); err != nil {
		return err
	}

	cc.SyncUsers()

	return nil
}

//Reload -
func (cc *Database) Reload() {
	if cc.DB != nil {
		cc.DB.Close()
	}

	// clean store
	for _, s := range cc.Stores.Users.LoadAll() {
		cc.Stores.Users.Delete(s.UserName)
	}

	cc.Init()

}

//SyncUsers -
func (cc *Database) SyncUsers() {
	if users, ok := cc.GetAll("users").([]Users); ok {
		for _, s := range users {
			cc.Stores.Users.Store(s.UserName, s)
		}
	} else {
		cc.Logger.Debug("users is empty, create default users")
		u := Users{
			UserName: "proxy",
			Password: cc.CreateHash("proxy"),
		}
		cc.Set("users", u.UserName, cc.Encode(u, nil), true)
		cc.SyncUsers()
	}
}

//ChangeUsers -
func (cc *Database) ChangeUsers(u Users) {
	cc.Set("users", u.UserName, cc.Encode(u, nil), true)
	cc.SyncUsers()
}

//DeleteUsers -
func (cc *Database) DeleteUsers(username string) {
	cc.Delete("users", username)
	cc.Stores.Users.Delete(username)
}

//Get -
func (cc *Database) Get(bucket, key string, autocreateBucket bool) (value []byte, err error) {

	if cc.DB == nil {
		return value, errors.New("Database not init")
	}
	if err = cc.DB.View(func(tx *nutsdb.Tx) error {
		var vals *nutsdb.Entry
		if vals, err = tx.Get(bucket, []byte(key)); err != nil {

			if !autocreateBucket {
				return err
			}

			if strings.Contains(err.Error(), "not found bucket") || strings.Contains(err.Error(), "key not found") {
				return nil
			}

			return nil
		}

		value = vals.Value

		return nil
	}); err != nil {
		return value, err
	}

	return value, err
}

//Set -
func (cc *Database) Set(bucket, key string, value []byte, autocreateBucket bool) (err error) {

	clean := func(err error) error {
		value = nil
		return err
	}

	if cc.DB == nil {
		return clean(errors.New("Database not init"))
	}

	if err := cc.DB.Update(func(tx *nutsdb.Tx) error {
		if err := tx.Put(bucket, []byte(key), value, 0); err != nil {
			return clean(err)
		}

		return nil

	}); err != nil {
		return clean(err)
	}

	return clean(err)

}

//Delete -
func (cc *Database) Delete(bucket, key string) (err error) {

	clean := func(err error) error {
		return err
	}

	if cc.DB == nil {
		return clean(errors.New("Database not init"))
	}

	if err := cc.DB.Update(func(tx *nutsdb.Tx) error {
		if err := tx.Delete(bucket, []byte(key)); err != nil {
			return clean(err)
		}

		return nil

	}); err != nil {
		return clean(err)
	}

	return clean(err)

}

//GetAll -
func (cc *Database) GetAll(bucket string) (res interface{}) {

	var users []Users

	if err := cc.DB.View(func(tx *nutsdb.Tx) error {
		entries, err := tx.GetAll(bucket)
		if err != nil {
			return err
		}

		for _, entry := range entries {
			switch bucket {
			case "users":
				var uss Users
				if err := cc.Decode(entry.Value, &uss); err == nil {
					users = append(users, uss)
				}
				break
			}
		}

		return nil
	}); err != nil {

	}

	switch bucket {
	case "users":
		if len(users) != 0 {
			res = users
		}
		break
	}

	return res
}

//Encode -
func (cc *Database) Encode(data interface{}, callback func(error)) []byte {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	if err != nil {
		if callback != nil {
			callback(err)
		}
		fmt.Println(err)
		buf.Reset()
		return nil
	}

	out := buf.Bytes()

	buf.Reset()

	if callback != nil {
		callback(nil)
	}

	return out
}

//Decode -
func (cc *Database) Decode(raw []byte, data interface{}) (err error) {
	buf := bytes.NewBuffer(raw)
	gob.NewDecoder(buf).Decode(data)
	if err != nil {
		raw = nil
		buf.Reset()
		return err
	}
	raw = nil
	buf.Reset()
	return err
}

//CreateHash -
func (cc *Database) CreateHash(key string) string {

	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()

	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

//UsersMap -
type UsersMap struct {
	sync.RWMutex
	internal map[string]Users
}

//NewUsersMap -
func NewUsersMap() *UsersMap {
	return &UsersMap{
		internal: make(map[string]Users),
	}
}

//Count -
func (rm *UsersMap) Count() (value int) {
	rm.RLock()
	result := len(rm.internal)
	rm.RUnlock()
	return result
}

//Load -
func (rm *UsersMap) Load(key string) (value Users, ok bool) {
	rm.RLock()
	result, ok := rm.internal[key]
	rm.RUnlock()
	return result, ok
}

//Delete -
func (rm *UsersMap) Delete(key string) {
	rm.Lock()
	delete(rm.internal, key)
	rm.Unlock()
}

//Store -
func (rm *UsersMap) Store(key string, value Users) {
	rm.Lock()
	rm.internal[key] = value
	rm.Unlock()
}

//LoadAll -
func (rm *UsersMap) LoadAll() (value []Users) {
	rm.RLock()
	for _, s := range rm.internal {
		value = append(value, s)
	}
	rm.RUnlock()
	return value
}
